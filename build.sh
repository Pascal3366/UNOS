#!/usr/bin/env bash

sh clean.sh

nasm -f elf32 -g -F dwarf src/asm/kernel.asm -o build/kernel.o

nasm -f elf32 -g -F dwarf src/asm/lowlevel.asm -o build/lowlevel.o

i686-elf-gcc -g -m32 -c src/c/main.c -o build/main.o -ffreestanding -O3 -Wall -Wextra -pedantic

i686-elf-gcc -g -m32 -c src/c/keyb.c -o build/keyb.o -ffreestanding -O3 -Wall -Wextra -pedantic

i686-elf-gcc -g -m32 -Wl,--32 -T src/linker/linker.ld -o build/unos.bin -ffreestanding -nostdlib build/lowlevel.o build/main.o build/keyb.o build/kernel.o -lgcc

# grub multiboot checksum

if grub-file --is-x86-multiboot build/unos.bin; then
  echo multiboot confirmed
else
  echo the file is not multiboot
fi

# next build bootable iso using grub

cp build/unos.bin build/ISOs/boot/unos.bin
cp src/grub.cfg build/ISOs/boot/grub/grub.cfg
grub-mkrescue -o build/ISOs/unos.iso build/ISOs
