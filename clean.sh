#!/usr/bin/env bash

rm build/boot.o

rm build/kernel.o

rm build/unos.bin

rm build/ISOs/unos.iso

rm build/ISOs/boot/unos.bin

rm build/ISOs/boot/grub/grub.cfg
